/**
 * Created by HEI on 29/8/2015.
 */

var concat = function (a, b) {
    return a + b
}
var apiRootURL = "/api";

module.exports = {


    webURL: {
        //General
        rootURL: "/",
        notFound: "/404",

        //Tutor
        showAllTutors: '/show-all-tutors',
        "tutorDetail": '/tutor-detail',
        editTutor: '/edit-tutor',
        finishEditTutor: '/finish-edit-tutor',
        submitCommentForTutor: '/submit-comment-for-tutor',
        marking: '/marking',


        //Client
        editClient: '/edit-client',
        finishEditClient: '/finish-edit-client',

        //Student
        "showAllStudents": "/show-all-clients",
        "removeStudent": '/remove-student',
        "successStudent": '/success-student',
        "manageStudent": '/manage-students',
        "processStudent": "/process-student",
        "studentDetail": '/post-detail',
        submitCommentForStudent: '/submit-comment',
        finishEditStudent: '/finish-edit-student',
        newStudent: '/new-student',
        editStudent: '/edit-student',
        createNewStudent: '/create-new-student',
        editStudentStatus: "/edit-student-status",

        //Authentication
        "register": "'/register'",
        "login": '/login',
        "logout": '/logout',
        "loginAgain": '/login-again',
        loginPage: '/login-page',
        registerPage: '/register-page'

    },

    apiURL: {

        //
        checkNameUsed: apiRootURL + "/check-name-can-use",

        //    User
        register: apiRootURL + '/register',

        login: apiRootURL + "/login",
        loginSuccess: apiRootURL + "/login-success",
        loginFail: apiRootURL + "/login-fail",

        getUser: apiRootURL + "/get-user",
        updateUser: apiRootURL + "/update-user",
        deleteUser: apiRootURL + "/delete-user",


        //    plan
        createPlan: apiRootURL + "/create-plan",
        getPlan: apiRootURL + "/get-plan",
        updatePlan: apiRootURL + "/update-plan",
        deletePlan: apiRootURL + "/delete-plan",

        getPlanList:apiRootURL+"/get-plan-list",

        //    Blog
        createBlog: apiRootURL + "/create-blog",
        getBlog: apiRootURL + "/get-blog",
        updateBlog: apiRootURL + "/update-blog",
        deleteBlog: apiRootURL + "/delete-blog",
        getBlogList:apiRootURL+"/get-blog-list",

        //    Planed event
        createPlanedEvent: apiRootURL + "/create-planed-event",
        getPlanedEvent: apiRootURL + "/get-planed-event",
        updatePlanedEvent: apiRootURL+"/update-planed-event",
        deletePlanedEvent: apiRootURL + "/delete-planed-event",
        getPlanListById:apiRootURL+"/get-planed-event-list",


        //    Event
        createEvent: apiRootURL + "/create-event",
        getEvent: apiRootURL + "/get-event",
        updateEvent: apiRootURL + "/update-event",
        deleteEvent: apiRootURL + "/delete-event",
        getEventList:apiRootURL+"/get-event-list",

        //    hotel
        hotel: apiRootURL + "/hotel",

    //Translate
        getTranslate:apiRootURL+"/get-translate"
    }


}