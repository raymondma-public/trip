/**
 * Created by HEI on 2015/8/10.
 */

var ejs = require('ejs');
var fs = require('fs');

var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;
var LocalStrategy = require('passport-local').Strategy;

var myUtil = require('./../utils/my-util.js');
var databaseData = require("../utils/database-data");
var myResponse = require("../utils/myResponse");
var configuration = require("../configuration");

//grab web
var request = require('request');
var cheerio = require('cheerio');

//controller
var planController = require("../utils/planController");
var blogController = require("../utils/blogController");
var eventController = require("../utils/eventController");
var planedEventController = require("../utils/planedEventController");
var userController = require("../utils/userController");

var validator = require('validator');

module.exports = function (router) {
    console.log("start api");

    //router.get('/api',
    //
    //    function (req, res) {
    //
    //        User.find({}).exec(function (err, user) {
    //            console.log(err);
    //            console.log(user);
    //            res.send(req.user);
    //        })
    //
    //    });


    passport.serializeUser(function (user, done) {
        console.log("user: " + user);
        done(null, user);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });


    //passport.use('local-login', new LocalStrategy({
    //        // by default, local strategy uses username and password, we will override with email
    //        usernameField: 'name',
    //        passwordField: 'password',
    //        passReqToCallback: true // allows us to pass back the entire request to the callback
    //    },
    //    function (req, name, password, done) { // callback with email and password from our form
    //
    //        console.log("name: " + name + "password: " + password);
    //        // find a user whose email is the same as the forms email
    //        // we are checking to see if the user trying to login already exists
    //        User.findOne({user_name: name}, function (err, user) {
    //            // if there are any errors, return the error before anything else
    //            if (err)
    //                return done(err);
    //
    //            // if no user is found, return the message
    //            if (!user)
    //                return done(null, false); // req.flash is the way to set flashdata using connect-flash
    //
    //            // if the user is found but the password is wrong
    //            //try {
    //            if (!user.validPassword(password)) {
    //                console.log("user.validPassword(password): " + user.validPassword(password));
    //                return done(null, false); // create the loginMessage and save it to session as flashdata
    //            }
    //            //} catch (err) {
    //            //}
    //            // all is well, return successful user
    //            return done(null, user);
    //        });
    //
    //    }));


    router.post(configuration.apiURL.login,
        //passport.authenticate('local-login',
        //
        //        {
        //
        //    //successRedirect: '/view_designers_own_project', // redirect to the secure profile section
        //    successRedirect: configuration.apiURL.loginSuccess, // redirect to the secure profile section
        //    failureRedirect: configuration.apiURL.loginFail, // redirect back to the signup page if there is an error
        //    failureFlash: true // allow flash messages
        //}),
        //    function (req, res) {
        //    res.charset = 'utf-8';
        //    console.log("req.user.user_name: " + req.user.user_name);
        //}
        function (req, res) {

            var body = "";
            req.on("data", function (data) {
                body += data.toString();
            })

            req.on("end", function () {
                    value = JSON.parse(body);
                    var name = value.name;
                    var password = value.password;
                    User.findOne({user_name: name}, function (err, user) {
                        if (err || !user) {
                            res.redirect(configuration.apiURL.loginFail);
                            return;
                        }

                        if (user.validPassword(password)) {
                            require('crypto').randomBytes(48, function (ex, buf) {
                                var token = buf.toString('hex');

                                var createTime = new Date().getTime();


                                user.login_token.push({token: token, create_time: createTime});
                                user.save(function (err) {
                                    if (err) {
                                        res.statusCode = 404;
                                        res.send(err);
                                    }
                                    var data = {
                                        message: "Login Successful",
                                        user: user
                                    }
                                    res.send(data);
                                })


                            });

                        } else {
                            res.redirect(configuration.apiURL.loginFail);
                        }

                    })

                }
            )
//res.send(req.body);

        }
    );

    //
    //router.get(configuration.apiURL.loginSuccess, function (req, res) {
    //    require('crypto').randomBytes(48, function (ex, buf) {
    //        var token = buf.toString('hex');
    //        var user = req.user;
    //        var createTime = new Date().getTime();
    //
    //
    //        user.login_token.push({token: token, create_time: createTime});
    //        user.save(function (err) {
    //            if (err) {
    //                res.statusCode = 404;
    //                res.send(err);
    //            }
    //            var data = {
    //                message: "Login Successful",
    //                user: req.user
    //            }
    //            res.send(data);
    //        })
    //
    //
    //    });
    //
    //})

    router.get(configuration.apiURL.loginFail, function (req, res) {
        res.statusCode = 404;
        res.send({message: "Login Fail"});
    })


    passport.use('bearer', new BearerStrategy(
        function (token, done) {
            console.log("Input token:" + token);
            User.findOne({"login_token.token": token}, function (err, user) {// "login_token.token": token
                console.log(err);
                console.log(user);
                if (err) {
                    console.log(err);
                    return done(err);
                }
                if (!user) {
                    return done(null, false);
                }
                console.log(user);
                return done(null, user, {scope: 'all'});
            });
        }
    ));


    router.get(configuration.apiURL.hotel, function (req, res) {

//var url='https://www.airbnb.com.hk/s/hong-kong?guests=&zoom=16&search_by_map=true&sw_lat=22.325676709666848&sw_lng=114.15365676715089&ne_lat=22.339517328040706&ne_lng=114.16515807940675&ss_id=y8mdvtav';

        //var url='https://www.airbnb.com.hk/s/hong-kong?guests=&zoom=16&search_by_map=true&sw_lat=22.325676709666848&sw_lng=114.15365676715089&ne_lat=22.339517328040706&ne_lng=114.16515807940675&ss_id=y8mdvtav';
        //var url='https://www.airbnb.com.hk';
        //var url="http://yahoo.com.hk"
        //var url ='https://modulus.io';
        //var url="https://news.ycombinator.com";


        var allPage = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        var imagesString;
        sendAllRequest();
        function sendAllRequest() {


            var url = 'https://www.airbnb.com.hk';
            var param = "/s/hong-kong?price_min=112&price_max=970&sw_lat=22.325676709666848&sw_lng=114.15365676715089&ne_lat=22.339517328040706&ne_lng=114.16515807940675&search_by_map=true&zoom=16&ss_id=y8mdvtav";

            for (var i = 0; i < allPage.length; i++) {

                var page = "&page=" + allPage[i];
                var wholeUrl = url + param + page;


                var options = {
                    url: wholeUrl,
                    headers: {
                        'User-Agent': 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0'
                    }
                };

                request(options, function (error, response, html) {


                    $ = cheerio.load(html);

                    var tempCount = 0;
                    var photoCount = 0;


                    $("img").each(function (i, elem) {
                        if ($(this).attr("src").indexOf("no_photos") > -1) {

                        } else {
                            photoCount++;
                            console.log(photoCount);
                            imagesString += "<img src='" + $(this).attr("src") + "'>";
                            imagesString += "<br>";
                        }
                        //console.log($(this).attr("src"));
                    })


                    $("div.media h3 a").each(function (i, elem) {
                        tempCount++;
                        if (tempCount < photoCount) {
                            imagesString += $(this).text() + "<br>";

                        }

                    });

                    //console.log($("img").attr("src"));

                    if (error) {
                        res.send(error);
                    }

                    finish(res);

                });

            }
        }

        var count = 0;

        function finish(res, context) {
            count++;
            console.log("count:" + count);
            if (count >= allPage.length) {
                res.send(imagesString);
            }

        }


    })


    router.get("/whatsapp", function (req, res) {

//var url='https://www.airbnb.com.hk/s/hong-kong?guests=&zoom=16&search_by_map=true&sw_lat=22.325676709666848&sw_lng=114.15365676715089&ne_lat=22.339517328040706&ne_lng=114.16515807940675&ss_id=y8mdvtav';

        //var url='https://www.airbnb.com.hk/s/hong-kong?guests=&zoom=16&search_by_map=true&sw_lat=22.325676709666848&sw_lng=114.15365676715089&ne_lat=22.339517328040706&ne_lng=114.16515807940675&ss_id=y8mdvtav';
        //var url='https://www.airbnb.com.hk';
        //var url="http://yahoo.com.hk"
        //var url ='https://modulus.io';
        //var url="https://news.ycombinator.com";


        var allPage = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        var imagesString;
        sendAllRequest();
        function sendAllRequest() {
            //var url="http://www.android.com/";
//var url="http://www.openrice.com/zh/hongkong";
       //     var url= 'https://www.airbnb.com.hk';
            var url = 'https://web.whatsapp.com';
            var param = "/s/hong-kong?price_min=112&price_max=970&sw_lat=22.325676709666848&sw_lng=114.15365676715089&ne_lat=22.339517328040706&ne_lng=114.16515807940675&search_by_map=true&zoom=16&ss_id=y8mdvtav";

            //for (var i = 0; i < allPage.length; i++) {

                //var page = "&page=" + allPage[i];
                var wholeUrl = url;// + param + page;


                var options = {
                    url: wholeUrl,
                    headers: {
                        //'User-Agent': 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0'
                        'User-Agent':"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
                    }
                };

                request(options, function (error, response, html) {

    console.log(html);
                    //$ = cheerio.load(html);
                    //
                    //var tempCount = 0;
                    //var photoCount = 0;


                    //$("img").each(function (i, elem) {
                    //    if ($(this).attr("src").indexOf("no_photos") > -1) {
                    //
                    //    } else {
                    //        photoCount++;
                    //        console.log(photoCount);
                    //        imagesString += "<img src='" + $(this).attr("src") + "'>";
                    //        imagesString += "<br>";
                    //    }
                    //    //console.log($(this).attr("src"));
                    //})
                    //
                    //
                    //$("div.media h3 a").each(function (i, elem) {
                    //    tempCount++;
                    //    if (tempCount < photoCount) {
                    //        imagesString += $(this).text() + "<br>";
                    //
                    //    }
                    //
                    //});

                    //console.log($("img").attr("src"));

                    if (error) {
                        res.send(error);
                    }
                    res.send(html);
                    //finish(res);

                });

            }
        //}

        //var count = 0;
        //
        //function finish(res, context) {
        //    count++;
        //    console.log("count:" + count);
        //    if (count >= allPage.length) {
        //        res.send(imagesString);
        //    }
        //
        //}


    })


    router.get(configuration.apiURL.checkNameUsed, function (req, res) {
        var name = req.query.name;
        if (!validator.isEmail(name)) {
            res.statusCode = 404;
            res.send(false);
            return;
        }
        console.log("checkName");
        User.find({user_name: name})
            .exec(function (err, users) {
                if (err) {
                    res.send(err);
                }

                if (users.length > 0) {
                    res.send(false);
                } else {
                    res.send(true);
                }

            })

    })


    router.post(configuration.apiURL.register,
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {
            var params = req.params;
            console.log("register");
            console.log(params.name);
            console.log(params.password);
            console.log(params.phone);
            //res.json(req.user);
            //res.send(req.body.name+" "+req.body.password+" "+req.body.phone);
            //res.send("Hello");
            databaseData.register(req, res, myResponse.send);
        });


    router.get(configuration.apiURL.getUser,
        passport.authenticate('bearer', {session: false}),
        function (req, res) {
            userController.getUser(req, res, myResponse.send);

        });


    //Plan
    router.post(configuration.apiURL.createPlan,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            //
            var body = "";
            req.on("data", function (data) {
                body += data.toString();
            })


            req.on("end", function () {
                planController.createPlan(req, res, myResponse.send, body);
            })
            //databaseData.createPlan(req, res, myResponse.send);
        });

    router.put(configuration.apiURL.updatePlan,
        passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            planController.updatePlan(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });
    router.get(configuration.apiURL.getPlan,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            planController.getPlan(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });


    router.get(configuration.apiURL.getPlanList,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            planController.getPlanList(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });

//    Blog
    router.post(configuration.apiURL.createBlog,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            blogController.createBlog(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });
    router.get(configuration.apiURL.getBlog,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            blogController.getBlog(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });
    router.get(configuration.apiURL.getBlogList,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            blogController.getBlogList(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });

//    Event
    router.post(configuration.apiURL.createEvent,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            eventController.createEvent(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });
    router.get(configuration.apiURL.getEvent,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            eventController.getEvent(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });


    router.get(configuration.apiURL.getEventList,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            eventController.getEventList(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });


//    PlanedEvent
    router.post(configuration.apiURL.createPlanedEvent,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            planedEventController.createPlanedEvent(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });
    router.get(configuration.apiURL.getPlanedEvent,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),
        function (req, res) {

            planedEventController.getPlanedEvent(req, res, myResponse.send);
            //databaseData.createPlan(req, res, myResponse.send);
        });

    //Translate
    router.get(configuration.apiURL.getTranslate,
        //passport.authenticate('bearer', {session: false}),
        //passport.authenticate('bearer', {session: false}),


        function (req, res) {
            var params = req.query;


            //var url="https://translate.google.com.hk/#";
            var sourceLanguage = params.sourceLanguage;
            var targetLanguage = params.targetLanguage;
            var words = params.words;
            var trans = require('translate-google-free');
            trans(words, sourceLanguage, targetLanguage, function (error, ret) {
                if (error) {
                    res.statusCode = 404;
                    send({data: "Cannot Get Translate"});
                }
                console.log(ret);
                res.send({data: ret});
            });

        });


    router.get("/grap-events", function (req, res) {

//var url='https://www.airbnb.com.hk/s/hong-kong?guests=&zoom=16&search_by_map=true&sw_lat=22.325676709666848&sw_lng=114.15365676715089&ne_lat=22.339517328040706&ne_lng=114.16515807940675&ss_id=y8mdvtav';

        //var url='https://www.airbnb.com.hk/s/hong-kong?guests=&zoom=16&search_by_map=true&sw_lat=22.325676709666848&sw_lng=114.15365676715089&ne_lat=22.339517328040706&ne_lng=114.16515807940675&ss_id=y8mdvtav';
        //var url='https://www.airbnb.com.hk';
        //var url="http://yahoo.com.hk"
        //var url ='https://modulus.io';
        //var url="https://news.ycombinator.com";


        var allPage = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        var imagesArr = [];
        var titleArr = [];
        sendAllRequest();
        function sendAllRequest() {


            var url = "http://www.openrice.com/zh/hongkong/restaurant/sr1.htm?";
            var param = "searchSort=31&region=0&district_id=l2003";

            for (var i = 0; i < allPage.length; i++) {

                var page = "&page=" + allPage[i];
                var wholeUrl = url + param + page;


                var options = {
                    url: wholeUrl,
                    headers: {
                        'User-Agent': 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0'
                    }
                };

                request(options, function (error, response, html) {


                    $ = cheerio.load(html);

                    var tempCount = 0;
                    var photoCount = 0;

                    var myImages = [];
                    var myTitles = [];
                    $("img[class=sr1_doorphoto]").each(function (i, elem) {
                        //$(this).attr("src");

                        var style = $(this).attr("style");
                        var link = style.replace("background:url(", "").replace(");", "");
                        //console.log(link);

                        //var regExp = /\(([^\)]+)\)/;
                        //var firstOutput = regExp.exec($(this).attr("style"));
                        //var link=regExp.exec(firstOutput);

                        imagesArr.push(link);
                        if (link != undefined) {
                            myImages.push(link);
                        }
                        //console.log(imagesArr);

                        //if ($(this).attr("src").indexOf("no_photos") > -1) {
                        //
                        //} else {
                        //    photoCount++;
                        //    console.log(photoCount);
                        //    imagesString += "<img src='" + $(this).attr("src") + "'>";
                        //    imagesString += "<br>";
                        //}
                        //console.log($(this).attr("src"));
                    })

                    $("a[class=poi_link]").each(function (i, elem) {

                        titleArr.push($(this).text());
                        if($(this).text()!=undefined){
                        myTitles.push($(this).text());
                        }
                    })


                    //var requestData=
                    //            {
                    //                imgs: myImages[i],
                    //                title: myTitles[i],
                    //                description: "This is the desciption of this location",
                    //                eventType: "eat",
                    //                "phone": 98765432,
                    //                "traffic": "MTR (Exit A)",
                    //                "tips": "This is my tips",
                    //                "access_token": "a1e502b3c0d3fc5274b3d572696caa719882cd696b91bef6b77f29bad9f6272f4296d8615f534d8ccc4c9b1299f6b78c"
                    //
                    //            };


                    //var requestData=
                    //{
                    //
                    //    "access_token":"a1e502b3c0d3fc5274b3d572696caa719882cd696b91bef6b77f29bad9f6272f4296d8615f534d8ccc4c9b1299f6b78c",
                    //    "title":myTitles[i],
                    //    "imgs":myImages[i],
                    //    "description":"This is my description for the blog",
                    //    "belongsToPlanId":"5637425f4afee3dc10a0be56"
                    //
                    //
                    //};

                    var requestData=
                    {
                        "noOfDays":8,
                        "access_token":"a1e502b3c0d3fc5274b3d572696caa719882cd696b91bef6b77f29bad9f6272f4296d8615f534d8ccc4c9b1299f6b78c",
                        "title":"Hong Kong Trip",
                        "description":"This trip is going to hong kong for description"
                    }

                    for (var k = 0; k < myTitles.length; k++) {
                        var options = {
                            url: "http://localhost:3000" + configuration.apiURL.createPlan,
                            method: "POST",
                            //headers: {
                            //    'User-Agent': 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/21.0',
                            //    name: 'content-type',
                            //    value: 'application/x-www-form-urlencoded'
                            //
                            //
                            //},
                            headers: {
                                "content-type": "application/json",
                            },
                            body: JSON.stringify(requestData)



                        };


                        request(options, function (error, response, html) {

                            console.log("=================================");
                            console.log(html);


                        });
                    }


                    if (error) {
                        res.send(error);
                    }

                    finish(res);

                });

            }
        }

        var count = 0;

        function finish(res, context) {
            count++;
            //console.log("count:" + count);

            var finalResult;
            for (var i = 0; i < imagesArr.length; i++) {
                finalResult += titleArr[i] + "</br>";
                finalResult += "<img src='" + imagesArr[i] + "'/></br>"
            }

            if (count >= allPage.length) {
                res.send(finalResult);
            }

        }


    })

}
