///**
// * Created by HEI on 7/6/2015.
// */
//
//
////db.connect();
//
//var passport = require('passport');
////BearerStrategy = require('passport-http-bearer').Strategy;
//var LocalStrategy = require('passport-local').Strategy;
//var password = require('password-hash-and-salt');
//var myUtil = require('./../utils/my-util.js');
//var databaseData = require("../utils/database-data");
//var myResponse = require("../utils/myResponse");
//
//var configuration = require("../configuration");
//
//module.exports = function (router) {
//
//
//
//
//    passport.serializeUser(function (user, done) {
//        console.log("user: " + user);
//        done(null, user);
//    });
//
//    passport.deserializeUser(function (id, done) {
//        User.findById(id, function (err, user) {
//            done(err, user);
//        });
//    });
//
//
//    router.post(configuration.webURL.register, function (req, res) {
//            if (req.user) {
//                // logged in
//                res.redirect('/');
//            }
//            res.charset = 'utf-8';
//
//            databaseData.register(req, res, myResponse.redirect);
//
//        }
//    );
//
//    router.get(configuration.webURL.removeStudent, function (req, res) {
//
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//
//        databaseData.removeStudent(req, res, myResponse.redirect);
//
//    });
//
//    router.get(configuration.webURL.processStudent, function (req, res) {
//
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//
//        databaseData.processStudent(req, res, myResponse.redirect);
//
//    });
//
//    router.get(configuration.webURL.editStudentStatus, function (req, res) {
//
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//
//
//        databaseData.editStudentStatus(req, res, myResponse.redirect);
//
//    });
//
//    router.get(configuration.webURL.successStudent, function (req, res) {
//
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//
//
//        databaseData.successStudent(req, res, myResponse.redirect);
//
//    });
//
//
//    passport.use(new LocalStrategy(
//        function (username, password, done) {
//            User.findOne({username: username}, function (err, user) {
//                if (err) {
//                    return done(err);
//                }
//                if (!user) {
//                    return done(null, false, {message: 'Incorrect username.'});
//                }
//                if (!user.validPassword(password)) {
//                    return done(null, false, {message: 'Incorrect password.'});
//                }
//                return done(null, user);
//            });
//        }
//    ));
//
//    passport.use('local-login', new LocalStrategy({
//            // by default, local strategy uses username and password, we will override with email
//            usernameField: 'name',
//            passwordField: 'password',
//            passReqToCallback: true // allows us to pass back the entire request to the callback
//        },
//        function (req, name, password, done) { // callback with email and password from our form
//
//            console.log("name: " + name + "password: " + password);
//            // find a user whose email is the same as the forms email
//            // we are checking to see if the user trying to login already exists
//            User.findOne({user_name: name}, function (err, user) {
//                // if there are any errors, return the error before anything else
//                if (err)
//                    return done(err);
//
//                // if no user is found, return the message
//                if (!user)
//                    return done(null, false); // req.flash is the way to set flashdata using connect-flash
//
//                // if the user is found but the password is wrong
//                //try {
//                if (!user.validPassword(password)) {
//                    console.log("user.validPassword(password): " + user.validPassword(password));
//                    return done(null, false); // create the loginMessage and save it to session as flashdata
//                }
//                //} catch (err) {
//                //}
//                // all is well, return successful user
//                return done(null, user);
//            });
//
//        }));
//
//    router.post(configuration.webURL.login, passport.authenticate('local-login', {
//
//        //successRedirect: '/view_designers_own_project', // redirect to the secure profile section
//        successRedirect: '/', // redirect to the secure profile section
//        failureRedirect: '/login-again', // redirect back to the signup page if there is an error
//        failureFlash: true // allow flash messages
//    }), function (req, res) {
//        res.charset = 'utf-8';
//        console.log("req.user.user_name: " + req.user.user_name);
//    });
//
//
//    router.get(configuration.webURL.manageStudent, function (req, res) {
//        res.charset = 'utf-8';
//        var data = {}
//        data = myUtil.initForAllPage(req, data, "page-manage-students", "管理學生");
//        //data = myUtil.addFieldForLoginNSubtitle(req, data, "學生");
//
//        if (req.user) {
//            // logged in
//        } else {
//            res.redirect('/login-again');
//        }
//
//        databaseData.showManageStudent(req, res, data, myResponse.render)
//
//    });
//
//
//    router.get(configuration.webURL.logout, function (req, res) {
//        res.charset = 'utf-8';
//        if (req.user) {
//            req.logout();
//        }
//        res.redirect('/');
//
//    });
//
//    router.get(configuration.webURL.loginAgain, function (req, res) {
//        res.charset = 'utf-8';
//        var data = {}
//        data = myUtil.initForAllPage(req, data, "page-login", "登入");
//        data.regSuccessful = req.query.regSuccessful;
//        if (req.user) {
//
//            res.redirect('/');
//        } else {
//            res.render('page-login', {data: data});
//        }
//
//    });
//
//
//    router.get(configuration.webURL.studentDetail, function (req, res) {
//        res.charset = 'utf-8';
//        var data = {};
//        data = myUtil.initForAllPage(req, data, 'client-detail', "學生資料");
//
//
//        databaseData.showStudentDetail(req, res, data, myResponse.render)
//    });
//
//
//    router.get(configuration.webURL.tutorDetail, function (req, res) {
//        res.charset = 'utf-8';
//        var data = {};
//        data = myUtil.initForAllPage(req, data, "tutor-detail", "導師資料");
//        //data = myUtil.addFieldForLoginNSubtitle(req, data, "導師資料");
//
//        databaseData.showTutorDetail(req, res, data, myResponse.render)
//    });
//
//
//    //router.get('/bookmark', function (req, res) {
//    //    if (!req.user) {
//    //        goLogin(req, res);
//    //        return;
//    //    }
//    //    res.charset = 'utf-8';
//    //    var data = {};
//    //    data = myUtil.addFieldForLogin(req, data);
//    //    res.render('page-bookmark', {data: data});
//    //});
//
//    router.get(configuration.webURL.editTutor, function (req, res) {
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//        res.charset = 'utf-8';
//        var data = {user: req.user};
//        data = myUtil.initForAllPage(req, data, "edit-tutor", "修改資料");
//        myResponse.render(res, data)
//
//    });
//
//
//    router.get(configuration.webURL.editClient, function (req, res) {
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//        res.charset = 'utf-8';
//        var data = {user: req.user};
//        data = myUtil.addFieldForLogin(req, data);
//
//        res.render('edit-client', {data: data});
//    });
//
//
//    router.get(configuration.webURL.loginPage, function (req, res) {
//        var data = {};
//        data = myUtil.initForAllPage(req, data, "page-login", "登入");
//
//        res.render('page-login', {data: data});
//    });
//
//
//    router.get(configuration.webURL.registerPage, function (req, res) {
//            if (req.user) {
//                res.redirect('/');
//                return;
//            }
//
//            if (req.query) {
//                var warning = req.query.warning;
//                console.log("res.query.warning:" + req.query.warning);
//            }
//            var data = {}
//            data = myUtil.initForAllPage(req, data, "page-register", "註冊");
//            data.warning = warning;
//
//            myResponse.render(res, data);
//
//        }
//    );
//
//    router.post(configuration.webURL.finishEditTutor, function (req, res) {
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//        var data = {};
//        data = myUtil.initForAllPage(req, data, "", "導師資料");
//        res.charset = 'utf-8';
//        databaseData.editTutor(req, res, data, myResponse.redirect)
//
//    });
//
//
//    router.post(configuration.webURL.finishEditClient, function (req, res) {
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//        res.charset = 'utf-8';
//        var data = {}
//        databaseData.editClient(req, res, data, myResponse.redirect)
//
//    });
//
//
//    router.post(configuration.webURL.submitCommentForStudent, function (req, res) {
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//        res.charset = 'utf-8';
//        var data = {}
//        databaseData.commentToClient(req, res, data, myResponse.redirect)
//
//    });
//
//
//    router.post(configuration.webURL.submitCommentForTutor, function (req, res) {
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//
//        var data = {}
//        databaseData.commentToTutor(req, res, data, myResponse.redirect)
//
//    });
//
//
//    router.post(configuration.webURL.finishEditStudent, function (req, res) {
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//        res.charset = 'utf-8';
//        var data = {}
//        databaseData.editStudent(req, res, data, myResponse.redirect)
//
//    });
//
//
//    router.get(configuration.webURL.newStudent, function (req, res) {
//
//        //if (!req.user) {
//        //    goLogin(req, res);
//        //    return;
//        //}
//
//        res.charset = 'utf-8';
//        var data = {}
//        data = myUtil.initForAllPage(req, data, 'new-student', "新增學生");
//        data.nextUrl = "/create-new-student";
//        data.student = {}
//        data.text = {}
//        data.text.finishBtn = "新增學生"
//        res.render('new-student', {data: data});
//    });
//
//
//    router.get(configuration.webURL.editStudent, function (req, res) {
//
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//        var data = {}
//        data = myUtil.initForAllPage(req, data, 'new-student', "修改學生");
//
//        databaseData.showEditStudent(req, res, data, myResponse.render);
//
//
//    });
//
//    router.post(configuration.webURL.marking, function (req, res) {
//
//        if (!req.user) {
//            goLogin(req, res);
//            return;
//        }
//
//        databaseData.marking(req, res, myResponse.redirect);
//
//    });
//
//
//    router.post(configuration.webURL.createNewStudent, function (req, res) {
//
//        res.charset = 'utf-8';
//        databaseData.newStudent(req, res, myResponse.redirect);
//
//    });
//
//    router.get(configuration.webURL.showAllTutors, function (req, res) {
//        var data = {}
//        data = myUtil.initForAllPage(req, data, "show-all-tutors", "導師");
//        res.charset = 'utf-8';
//
//        databaseData.getAllTutors(res, data, myResponse.render);
//    });
//
//    router.get(configuration.webURL.showAllStudents, function (req, res) {
//        var data = {}
//        data = myUtil.initForAllPage(req, data, 'show-all-students', "學生");
//        res.charset = 'utf-8';
//
//        console.log("configuration.apiURL.getMoreStudentsPost: "+configuration.apiURL.getMoreStudentsPost);
//
//        console.log("data:" + data);
//
//        databaseData.getAllStudents(res, data, myResponse.render);
//
//    });
//
//
//    router.get(configuration.webURL.rootURL, function (req, res) {
//        console.log("補習");
//        res.charset = 'utf-8';
//        myResponse.redirect(res, '/show-all-clients');
//        //res.redirect('/show-all-clients');
//    });
//
//
//    var goLogin = function (req, res) {
//        res.redirect('/login-again');
//    }
//
//
//    router.get(configuration.webURL.notFound, function (req, res) {
//        var data = {}
//        data = myUtil.addFieldForLoginNSubtitle(req, data, "404");
//        res.charset = 'utf-8';
//        res.status(404);
//        res.render('page-404', {data: data});
//    });
//
//    router.use(function (req, res) {
//        var data = {}
//        data = myUtil.addFieldForLoginNSubtitle(req, data, "404");
//        res.status(404);
//        res.render('page-404', {data: data});
//    });
//}