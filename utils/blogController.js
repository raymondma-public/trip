/**
 * Created by RaymondMa on 2/11/2015.
 */
var mongoose = new require('mongoose');
var myUtil = require('./my-util');
exports.createBlog = function (req, res, endFunc) {
    //var params = req.body
    //var imgs = [];
    //
    //if (myUtil.isArray(params.imgs)) {
    //    for (var i = 0; i < params.imgs.length; i++) {
    //        imgs.push({srcURL: params.imgs[i]});
    //    }
    //} else {
    //    imgs.push({srcURL: params.imgs});
    //}
    //var description = params.description;
    //var belongsToPlanId = params.belongsToPlanId;
    //var rating = [];
    //var viewed = [];
    //
    //var token = req.body.access_token;
    //
    //var date = new Date().getTime();


    var body = "";
    req.on("data", function (data) {
        body += data.toString();
    })


    req.on("end", function () {

        value = JSON.parse(body);

        var params = value

        var imgs = [];
        var title=params.title;
        if (myUtil.isArray(params.imgs)) {
            for (var i = 0; i < params.imgs.length; i++) {
                imgs.push({srcURL: params.imgs[i]});
            }
        } else {
            imgs.push({srcURL: params.imgs});
        }
        var description = params.description;
        var belongsToPlanId = params.belongsToPlanId;
        var rating = [];
        var viewed = [];



        var token = value.access_token;

        var date = new Date().getTime();


        User.findOne({"login_token.token": token}, function (err, user) {
            if (err || !user) {
                res.statusCode = 404;
                res.send({message:"unauthorized"});
                return
            } else {

                var tempRecord = new Blog({

                    _id: mongoose.Types.ObjectId(),
                    imgs: imgs,
                    description: description,
                    title:title,
                    //user_dob: dob,
                    rating: rating,
                    viewed: viewed,
                    createTime: date,
                    createdBy: user._id

                });


                tempRecord.save(function (err) {
                    if (err) {
                        console.log('Error on save! ' + err);
                        res.statusCode = 404;
                        endFunc(res, null, null, {message: "Error:" + err.errmsg,})
                        //res.redirect("register-page?warning=" + true);
                        //endFunc(res, "/login-again?regSuccessful=true", null, {message: "OK"});

                    } else {

                        Plan.findOne({_id: belongsToPlanId}, function (err, plan) {
                            plan.blogs.push(tempRecord._id);

                            plan.save(function (err) {
                                if (err) {
                                    console.log('Error on save! ' + err);
                                }
                            })
                        })
                        //res.send("OK");

                        endFunc(res, null, null, {message: "OK"});


                    }
                });
            }
        })

    })
}


exports.getBlog = function (req, res, endFunc) {
    var params = req.query;
    console.log("_id:" + params.id);
    Blog.findOne({_id: params.id}, function (err, blog) {//_id:params.id
        if (err || !blog) {
            res.statusCode = 404;
            res.send({message: "Not Found"});
        } else {
            endFunc(res, null, null, blog);
        }

    })
}


exports.getBlogList = function (req, res, endFunc) {
    var params = req.query;
    console.log("_id:" + params.id);



    Blog.find({}, function (err, blog) {//_id:params.id
        if (err || !blog) {
            res.statusCode = 404;
            res.send({message: "Not Found"});
        } else {


            var tempBlogs=JSON.parse(JSON.stringify(blog))

            var bannerImages=[];
            var top3Blogs=[];

            var countBanner=0;
            for(var i=0;i<tempBlogs.length;i++){
                var tempImgs=tempBlogs[i].imgs
                for(var j=0;j<tempImgs.length;j++){

                    if(countBanner<5){
                        bannerImages.push(tempImgs[j].srcURL);
                        countBanner++;
                    }else{
                        break
                    }

                }
            }

            var top3Counter=0;
            for(var i=0;i<tempBlogs.length;i++){

                if(top3Counter<3){
                    console.log(i);
                    top3Blogs.push(tempBlogs[i]);
                    top3Counter++;
                }else{
                    break;
                }
            }
            //console.log(tempBlogs);





            endFunc(res, null, null, {data: tempBlogs,top3Blogs:top3Blogs,bannerImages:bannerImages});
        }

    })
}