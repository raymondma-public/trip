/**
 * Created by HEI on 21/5/2015.
 */
var time = require("./time.js");
var configuration = require("../configuration");

exports.addFieldForLogin = function (req, object) {
    object["user"] = req.user;
    object.title = "Tu+"
    return object;
}

exports.addFieldForLoginNSubtitle = function (req, object, subtitle) {
    object = this.addFieldForLogin(req, object);
    object.title = object.title + " | " + subtitle;
    return object
}

exports.isContain = function (element, array) {
    for (var i in array) {
        if (array[i]._id == element._id) {
            return true;
        }
    }
}

exports.sendNotification = function (notifyPeopleArr, student_post, exceptCase, creator, content) {
    console.log("sendNotification");

    if (!notifyPeopleArr) {
        notifyPeopleArr = [];
    }
    console.log("notifyPeopleArr: " + notifyPeopleArr);
    var tempNotification = {
        notify_content: content,
        notify_date: time.getCurrentTimestamp(),
        notify_post: student_post,
        notify_creator: creator
    }


    for (var i = 0; i < notifyPeopleArr.length; i++) {
        console.log("i: " + i);
        console.log("notifyPeopleArr[i]: " + notifyPeopleArr[i]);
        if (notifyPeopleArr[i]._id != exceptCase._id) {
            notifyPeopleArr[i].notify_list.push(tempNotification);
        }

    }
    var idArr = [];
    for (var i = 0; i < notifyPeopleArr.length; i++) {
        idArr[i] = notifyPeopleArr[i]._id
    }


    console.log("idArr:" + idArr);

    User.update(
        {_id: {$in: idArr}},
        //{_id: idArr[0]},
        {$push: {"notify_list": tempNotification}},
        {upsert: true},
        function (err, numAffected) {
            console.log("sendNotification return");
            if (err) {
                console.log("err:" + err);
                return err
            } else {
                console.log("==================================");
                console.log("numAffected:" + numAffected);
            }
        }
    )

}

exports.processFunc = function (initFunc, mainFunc, finishFunc) {
    initFunc();
    mainFunc();
    finishFunc();
}

exports.initForAllPage = function (req, data, url, subtitle) {
    this.addFieldForLoginNSubtitle(req, data, subtitle);
    data.url = url;
    console.log("configuration:"+configuration);
    data.config=configuration;
    return data;
}


exports.finishFunc = function () {

}

exports.isArray=function(myVar){
    if(Array.isArray(myVar)|| (myVar && myVar.constructor === Array)  || myVar instanceof Array ) {
        return true
    }else{
        return false
    }

}


exports.createMessage=function(myVar){
  return {message:myVar}

}

