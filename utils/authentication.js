/**
 * Created by HEI on 25/8/2015.
 */

var bcrypt = require('bcrypt-nodejs');

exports.generateHash=function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
}