/**
 * Created by HEI on 25/8/2015.
 */
var mongoose = new require('mongoose');
var myUtil = require('./my-util.js');
var auth = require("./authentication");
var validator = require('validator');

//
//
//exports.getAllStudents = function (res, data, endFunc) {
//    Student.find({})
//        .select()
//        .populate('_creator')
//        .sort({create_time: 'desc'})
//        .exec(function (err, doc) {
//            console.log("data:" + data);
//            console.log("getAllStudents()");
//            if (err) {
//                res.statusCode = 404;
//
//                console.log("err: " + err);
//            }
//
//            //data.user = req.user;
//            data.doc = doc;
//            //return data;
//
//            console.log("data:" + data.url);
//            endFunc(res, data);
//
//        });
//}
//
//
//exports.getAllTutors = function (res, data, endFunc) {
//    User.find({'user_type': 'Tutor'})
//        .select()
//        .sort({user_create_time: "desc"})
//        .exec(function (err, doc) {
//            if (err) {
//                res.statusCode = 404;
//                console.log("err: " + err);
//                return;
//            }
//            var array;
//            if (!( doc instanceof Array)) {
//                array = [doc];
//            } else {
//                array = doc
//            }
//
//
//            data.doc = doc
//
//            console.log("data:" + data.url);
//            endFunc(res, data);
//        });
//}
//
//exports.showManageStudent = function (req, res, data, endFunc) {
//    Student.find({"_creator": req.user._id})//
//        .select()
//        .exec(function (err, doc) {
//            //console.log("============================");
//            //console.log("doc: " + doc);
//            if (err) {
//                res.statusCode = 404;
//                console.log("err: " + err);
//            }
//
//
//            var array
//            if (doc instanceof Array) {
//                array = doc
//            } else {
//                array = [doc]
//            }
//
//            data.doc = array;
//            //console.log("doc: " + data.doc);
//            endFunc(res, data);
//        })
//}
//
//
//exports.showTutorDetail = function (req, res, data, endFunc) {
//    console.log("req.query.id: " + req.query.id);
//    var id = req.query.id;
//
//    User.find({'_id': id, user_type: 'Tutor'})
//        .populate('comment.comment_creator')
//        .exec(function (err, doc) {
//            if (err) {
//                res.statusCode = 404;
//                console.log("err: " + err);
//            }
//
//            if (doc.length <= 0) {
//                res.statusCode = 404;
//                res.render('page-404', {data: data});
//            }
//
//            data.doc = doc;
//
//            res.render('tutor-detail', {data: data});
//        })
//}
//
//exports.showStudentDetail = function (req, res, data, endFunc) {
//    data.user = req.user;
//    console.log("req.query.id: " + req.query.id);
//    var id = req.query.id;
//
//    Student.find({'_id': id})
//        .populate('_creator')
//        .populate('comment.comment_creator')
//        .exec(function (err, doc) {
//            if (err) {
//                res.statusCode = 404;
//                console.log("err: " + err);
//                return err
//            }
//
//            if (doc.length <= 0) {
//                res.statusCode = 404;
//                res.render('page-404', {data: data});
//                return
//            }
//
//            data.doc = doc;
//
//            endFunc(res, data, null, data);
//        })
//}
//
//
//exports.newStudent = function (req, res, endFunc) {
//    //var studentArea = {};
//    //var areaId = req.body.location;
//    //studentArea.put(areaId);
//    var tutorGenderRequirement = req.body.gender;
//    //var studentAvailableDays = req.body.studentAvailableDays;
//    var studentGender = req.body.gender;
//
//    var studentLevel = req.body.eduLevel;
//
//
//    var studentExpectedHrPrice = req.body.studentExpectedPrice;
//    if (!validator.isNumeric(studentExpectedHrPrice)) {
//        res.statusCode = 404;
//    }
//    //var studentSubject = req.body.studentSubject;
//
//    var checkboxDays = req.body.checkboxDays;
//
//
//    var core = req.body.core;
//    var elective = req.body.elective;
//
//    var hkIsland = req.body.checkboxHKIsland;
//    var kowloon = req.body.checkboxKowloon;
//    var newT = req.body.checkboxNewT;
//    var island = req.body.checkboxIsland;
//    var tutorGender = req.body.tutorGender;
//    var otherRequirement = req.body.otherRequirement;
//
//
//    var startTime = req.body.dtp_start;
//    if (!validator.isDate(startTime)) {
//        res.statusCode = 404;
//    }
//    var endTime = req.body.dtp_end;
//    if (!validator.isDate(endTime)) {
//        res.statusCode = 404;
//    }
//
//    var startTimeObj = (new Date(startTime.split(' ').join('T')));
//    var endTimeObj = (new Date(endTime.split(' ').join('T')));
//    var startTimeStamp = startTimeObj.getTime();
//    var endTimeStamp = endTimeObj.getTime();
//    var createTime = (new Date()).getTime()
//
//    var phone = req.body.contactNo;
//    if (!validator.isNumeric(phone)) {
//        res.statusCode = 404;
//    }
//
//    var otherRequirement = req.body.otherRequirement;
//    console.log("======================================");
//    console.log("otherRequirement: " + otherRequirement);
//
//    console.log("startTimeStamp: " + startTimeStamp);
//    console.log("endTimeStamp: " + endTimeStamp);
//
//    console.log('core: ' + core);
//    console.log("elective: " + elective);
//    console.log("checkboxDays: " + checkboxDays);
//
//    console.log("hkIsland: " + hkIsland)
//    console.log("kowloon: " + kowloon);
//    console.log("newT: " + newT);
//    console.log("island: " + island);
//
//
//    //console.log("areaId: " + areaId);
//    console.log("tutorGenderRequirement: " + tutorGenderRequirement);
//    //console.log("studentAvailableDays: " + studentAvailableDays);
//    console.log("studentGender: " + studentGender);
//    console.log("studentLevel: " + studentLevel);
//    console.log("studentExpectedHrPrice: " + studentExpectedHrPrice);
//    //console.log("studentSubject: " + studentSubject);
//    var tempDays = [];
//    if (checkboxDays) {
//        if (typeof checkboxDays === 'string') {
//            tempDays.push({id: checkboxDays});
//        } else {
//            for (var i = 0; i < checkboxDays.length; i++) {
//                tempDays.push({id: checkboxDays[i]});
//            }
//        }
//    }
//    console.log("otherRequirement: " + otherRequirement);
//    var areaObjs = [];
//
//    if (hkIsland) {
//        if (typeof hkIsland === 'string') {
//            areaObjs.push({id: hkIsland});
//        } else {
//            for (var i = 0; i < hkIsland.length; i++) {
//                areaObjs.push({id: hkIsland[i]});
//            }
//        }
//    }
//
//    if (kowloon) {
//        if (typeof kowloon === 'string') {
//            areaObjs.push({id: kowloon});
//        } else {
//            for (var i = 0; i < kowloon.length; i++) {
//                areaObjs.push({id: kowloon[i]});
//            }
//        }
//
//    }
//
//    if (newT) {
//        if (typeof newT === 'string') {
//            areaObjs.push({id: newT});
//        } else {
//            for (var i = 0; i < newT.length; i++) {
//                areaObjs.push({id: newT[i]});
//            }
//
//        }
//
//    }
//    if (island) {
//        if (typeof island === 'string') {
//            areaObjs.push({id: island});
//        } else {
//            for (var i = 0; i < island.length; i++) {
//                areaObjs.push({id: island[i]});
//            }
//        }
//    }
//    console.log("areaObjs: " + areaObjs.toString());
//
//    var tempSubject = [];
//    if (core) {
//        if (typeof core === 'string') {
//            tempSubject.push({id: core});
//        } else {
//            for (var i = 0; i < core.length; i++) {
//                tempSubject.push({id: core[i]});
//            }
//        }
//    }
//
//    if (elective) {
//        if (typeof elective === 'string') {
//            tempSubject.push({id: elective});
//        } else {
//            for (var i = 0; i < elective.length; i++) {
//                tempSubject.push({id: elective[i]});
//            }
//        }
//    }
//    var tempRecord = new Student({
//
//        _id: mongoose.Types.ObjectId(),
//        student_area: areaObjs,
//        tutor_gender_requirement: tutorGender,
//        student_days: tempDays,
//        student_gender: studentGender,
//        student_level: studentLevel,
//        student_expected_hr_price: studentExpectedHrPrice,
//        student_subject: tempSubject,
//        other_requirement: otherRequirement,
//        single_student_phone: phone,
//        create_time: createTime
//    });
//    if (req.user) {
//        tempRecord._creator = req.user._id;
//    }
//    if (startTimeStamp && !isNaN(startTimeStamp)) {
//        tempRecord.start_time = startTimeStamp;
//
//    }
//    if (endTimeStamp && !isNaN(endTimeStamp)) {
//        tempRecord.end_time = endTimeStamp;
//    }
//
//    if (req.user) {
//        tempRecord._creator = req.user._id;
//    }
//    tempRecord.save(function (err) {
//        if (err) {
//            res.statusCode = 404;
//            console.log('Error on save! ' + err);
//        } else {
//
//            console.log("endFunc");
//            endFunc(res, "/");
//
//        }
//    });
//
//
//}


exports.register = function (req, res, endFunc) {


    var body = "";
    req.on("data", function (data) {
        body += data.toString();
    })

    req.on("end", function () {
            value = JSON.parse(body);
            var name = value.name;
            var tempPassword = value.password;
            var phone=value.phone;

            //var params=req.body;

            if ( !validator.isEmail(name)) {
                res.statusCode = 404;
                res.send({message:"Not Email"})
                return
            }


            //var role = req.body.role;
            //var phone = params.phone;



            if (phone && !validator.isNumeric(phone)) {
                res.statusCode = 404;
                res.send({message:"Invalid Phone"})
                return
            }
            //var dob = req.body.dob;
            var date = new Date().getTime();

            console.log(name);
            console.log(tempPassword);
            //console.log(role);
            console.log(phone);
            //console.log(dob);
            console.log(date);


            var tempRecord = new User({
                //_creator: req.user.id,
                _id: mongoose.Types.ObjectId(),
                user_name: name,
                //user_type: role,
                user_phone: phone,
                //user_dob: dob,
                user_create_time: date

            });


            tempRecord.user_password_hash = auth.generateHash(tempPassword);


            tempRecord.save(function (err) {
                if (err) {
                    console.log('Error on save! ' + err);
                    res.statusCode = 404;
                    res.send({message:"Cannot Register"});
                    //endFunc(res, "/login-again?regSuccessful=true", null, {message: "Error:"+err.errmsg,})
                    //res.redirect("register-page?warning=" + true);
                    //endFunc(res, "/login-again?regSuccessful=true", null, {message: "OK"});
                } else {
                    //res.send("OK");

                    endFunc(res, "/login-again?regSuccessful=true", null, {message: "OK"});


                }
            });




        }
    )



}








exports.removeStudent = function (req, res, endFunc) {

    var studentID = req.query.id;
    console.log("studentID:" + studentID);
    Student.findOne({_id: studentID})
        .exec(function (err, student) {

            var tempStudent = student
            console.log("==============remove==========");
            console.log("student.status:" + tempStudent.status);
            console.log("student.status:" + tempStudent._id);
            console.log("tempStudent:" + tempStudent)
            if (tempStudent._creator != req.user._id) {
                res.statusCode = 404;
                res.redirect("/");
                return;
            }

            tempStudent.status = "removed";
            tempStudent.save(function (err) {
                if (err) {
                    return err
                }
                console.log("student.status:" + tempStudent.status);

                endFunc(res, "/manage-students");
            });


        })
}



exports.processStudent = function (req, res, endFunc) {

    var studentID = req.query.id;
    console.log("studentID:" + studentID);
    Student.findOne({_id: studentID})
        .exec(function (err, student) {

            var tempStudent = student
            console.log("==============process==========");
            console.log("student.status:" + tempStudent.status);
            console.log("student.status:" + tempStudent._id);
            console.log("tempStudent:" + tempStudent)
            if (tempStudent._creator != req.user._id) {
                res.statusCode = 404;
                res.redirect("/");
                return;
            }

            tempStudent.status = "processing";
            tempStudent.save(function (err) {
                if (err) {
                    return err
                }
                console.log("student.status:" + tempStudent.status);

                endFunc(res, "/manage-students");
            });


        })
}

exports.editStudentStatus = function (req, res, endFunc) {
    var statusValues = ["removed", "successful", "activate"]
    var studentID = req.query.id;
    var status = req.query.status;
    if (statusValues.indexOf(studentID) >= 0) {
        var studentStatus = status;
    } else {
        res.statusCode = 404;
    }


    console.log("studentID:" + studentID);
    Student.findOne({_id: studentID})
        .exec(function (err, student) {

            var tempStudent = student
            console.log("==============remove==========");
            console.log("student.status:" + tempStudent.status);
            console.log("student.status:" + tempStudent._id);
            console.log("tempStudent:" + tempStudent)
            if (tempStudent._creator != req.user._id) {
                res.redirect("/");
                return;
            }

            tempStudent.status = studentStatus
            tempStudent.save(function (err) {
                if (err) {
                    res.statusCode = 404;
                }
                console.log("student.status:" + tempStudent.status);

                endFunc(res, "/manage-students");
            });


        })
}


exports.successStudent = function (req, res, endFunc) {

    var studentID = req.query.id;
    console.log("studentID:" + studentID);
    Student.findOne({_id: studentID})
        .exec(function (err, student) {

            var tempStudent = student
            console.log("==============success==========");
            console.log("student.status:" + tempStudent.status);
            console.log("student.status:" + tempStudent._id);
            console.log("tempStudent:" + tempStudent)
            if (tempStudent._creator != req.user._id) {
                res.statusCode = 404;
                res.redirect("/");
                return;
            }

            tempStudent.status = "success";
            tempStudent.save(function (err) {
                if (err) {
                    res.statusCode = 404;

                }
                console.log("student.status:" + tempStudent.status);

                endFunc(res, "/manage-students");
            });


        })
}


exports.marking = function (req, res, endFunc) {
    var tutorId = req.body.tutorId;

    User.findOne({_id: tutorId})
        .exec(function (err, user) {
            res.charset = 'utf-8';

            tempMark = {
                mark: req.body.mark,
                mark_creator: req.user
            }

            if (user.mark) {
                user.mark.push(tempMark);
            } else {
                user.mark = [tempMark];
            }

            user.save(function (err) {
                if (err) {
                    res.statusCode = 404;

                }

                endFunc(res, "/tutor-detail?id=" + tutorId);

            });

        })

}

exports.showEditStudent = function (req, res, data, endFunc) {
    var studentID = req.query.id;

    Student.findOne({_id: studentID, _creator: req.user._id})
        .populate("_creator")
        .exec(function (err, student) {
            console.log("student._creator:" + student._creator._id);
            console.log("req.user._id:" + req.user._id);

            res.charset = 'utf-8';
            if (student._creator._id != req.user._id) {
                res.statusCode = 404;
                res.redirect("/");
                return;
            }


            data.student = student
            data.nextUrl = "/finish-edit-student";
            data.text = {}
            data.text.finishBtn = "Edit"


            endFunc(res, data);
        })
}


exports.editTutor = function (req, res, data, endFunc) {
    console.log("req.body.location: " + req.body.location);

    tempPassword = req.body.password
    tempRetypePassword = req.body.retypePassword



    tempLastName = req.body.lastName
    tempFirstName = req.body.firstName

    tempGender = req.body.gender

    tempTel = req.body.tel

    if(validator.isNumeric(tempTel)){
        res.statusCode=404;
    }


    tempLocation = req.body.location


    tempSubject = req.body.subject;

    tempTutorialLevel = req.body.tutorialLevel

    tempEduLevel = req.body.eduLevel

    tempPublicExamResult = req.body.publicExamResult

    tempTutorialDays = req.body.tutorialDays

    tempOtherDescriptions = req.body.otherDescriptions

    console.log(tempLastName);
    console.log(tempFirstName);
    console.log(tempGender);
    console.log(tempTel);
    console.log(tempLocation);
    console.log(tempTutorialLevel);
    console.log(tempSubject);
    console.log(tempEduLevel);
    console.log(tempPublicExamResult);
    console.log(tempTutorialDays);
    console.log(tempOtherDescriptions);


    //console.log("user: "+user);
    User.update({_id: req.user._id}, {
        $set: {
            user_last_name: tempLastName,
            user_first_name: tempFirstName,
            user_phone: tempTel,
            tutor_area: tempLocation,
            //user.tutor_expected_hr_salary
            tutor_teach_level: tempTutorialLevel,
            tutor_gender: tempGender,
            tutor_subject: tempSubject,
            tutor_public_exam_detail: tempPublicExamResult,
            tutor_available_days: tempTutorialDays,

            tutor_other_detail: tempOtherDescriptions,
            tutor_edu_level: tempEduLevel


        }
    }).exec(function (err) {
        console.log("message:" + err);
        endFunc(res, "/show-all-tutors");
    })
}

exports.editClient = function (req, res, data, endFunc) {


    console.log("req.body.location: " + req.body.location);

    var phone = req.body.tel;

    if(validator.isNumeric(phone)){
        res.statusCode=404;
    }

    User.update({_id: req.user._id}, {
        $set: {
            user_phone: phone
        }
    }).exec(function (err) {
        if (err) {
            res.statusCode = 404;
        }
        console.log("message:" + err);
        endFunc(res, "/show-all-tutors");
    })


}


exports.commentToClient = function (req, res, data, endFunc) {
    var postId = req.body.postId;
    var userId = req.body.userId;
    var content = req.body.comment_content;
    var date = new Date().getTime();

    //console.log("date:" + date);

    tempComment = {
        content: content,
        comment_creator: userId,
        comment_time: date
    };
    console.log("tempComment:" + tempComment);

    Student.findOne({_id: postId})
        .populate("_creator")
        .populate("follow_list")
        .exec(function (err, student) {


            //console.log("student: " + student);
            //console.log("_creator" + student._creator);
            //console.log("follow_list" + student.follow_list);
            student.comment.push(tempComment);
            if (!myUtil.isContain(req.user, student.follow_list)) {
                student.follow_list.push(req.user._id);
            }

            myUtil.sendNotification(student.follow_list, student, req.user._id, req.user, content);

            student.save(function (err) {

                if (err) {
                    return err;
                }

                endFunc(res, "/post-detail?id=" + postId);

            })


        }
    )

}


exports.commentToTutor = function (req, res, data, endFunc) {
    res.charset = 'utf-8';
    //console.log("req.body.location: " + req.body.location);

    var postId = req.body.postId;
    var userId = req.body.userId;
    var content = req.body.comment_content;

    var commentTime = (new Date()).getTime();
    console.log("commentTime:" + commentTime);
    tempComment = {
        content: content,
        comment_creator: userId,
        comment_time: commentTime
    };
    console.log("tempComment:" + tempComment);

    User.findOne({_id: postId}, function (err, tutor) {
            console.log("tutor: " + tutor);
            tutor.comment.push(tempComment);
            tutor.save(function (err) {
                if (err) {
                    console.log(err);
                    return err;
                }
                console.log("redirecting");
                endFunc(res, "/tutor-detail?id=" + postId);
            })
        }
    )
}

exports.editStudent = function (req, res, data, endFunc) {
    var tutorGenderRequirement = req.body.gender;
    //var studentAvailableDays = req.body.studentAvailableDays;
    var studentGender = req.body.gender;
    var studentLevel = req.body.eduLevel;
    var studentExpectedHrPrice = req.body.studentExpectedPrice;
    if(validator.isNumeric(studentExpectedHrPrice)){
        res.statusCode=404;
    }
    //var studentSubject = req.body.studentSubject;

    var checkboxDays = req.body.checkboxDays;


    var core = req.body.core;
    var elective = req.body.elective;

    var hkIsland = req.body.checkboxHKIsland;
    var kowloon = req.body.checkboxKowloon;
    var newT = req.body.checkboxNewT;
    var island = req.body.checkboxIsland;
    var tutorGender = req.body.tutorGender;
    var otherRequirement = req.body.otherRequirement;


    var startTime = req.body.dtp_start;

    if(validator.isDate(startTime)){
        res.statusCode=404;
    }

    var endTime = req.body.dtp_end;
    if(validator.isDate(endTime)){
        res.statusCode=404;
    }
    var startTimeObj = (new Date(startTime.split(' ').join('T')));
    var endTimeObj = (new Date(endTime.split(' ').join('T')));
    var startTimeStamp = startTimeObj.getTime();
    var endTimeStamp = endTimeObj.getTime();


    var otherRequirement = req.body.otherRequirement;
    console.log("======================================");
    console.log("otherRequirement: " + otherRequirement);

    console.log("startTimeStamp: " + startTimeStamp);
    console.log("endTimeStamp: " + endTimeStamp);

    console.log('core: ' + core);
    console.log("elective: " + elective);
    console.log("checkboxDays: " + checkboxDays);

    console.log("hkIsland: " + hkIsland)
    console.log("kowloon: " + kowloon);
    console.log("newT: " + newT);
    console.log("island: " + island);


    //console.log("areaId: " + areaId);
    console.log("tutorGenderRequirement: " + tutorGenderRequirement);
    //console.log("studentAvailableDays: " + studentAvailableDays);
    console.log("studentGender: " + studentGender);
    console.log("studentLevel: " + studentLevel);
    console.log("studentExpectedHrPrice: " + studentExpectedHrPrice);
    //console.log("studentSubject: " + studentSubject);
    var tempDays = [];
    if (checkboxDays) {
        if (typeof checkboxDays === 'string') {
            tempDays.push({id: checkboxDays});
        } else {
            for (var i = 0; i < checkboxDays.length; i++) {
                tempDays.push({id: checkboxDays[i]});
            }
        }
    }
    console.log("otherRequirement: " + otherRequirement);
    var areaObjs = [];

    if (hkIsland) {
        if (typeof hkIsland === 'string') {
            areaObjs.push({id: hkIsland});
        } else {
            for (var i = 0; i < hkIsland.length; i++) {
                areaObjs.push({id: hkIsland[i]});
            }
        }
    }

    if (kowloon) {
        if (typeof kowloon === 'string') {
            areaObjs.push({id: kowloon});
        } else {
            for (var i = 0; i < kowloon.length; i++) {
                areaObjs.push({id: kowloon[i]});
            }
        }

    }

    if (newT) {
        if (typeof newT === 'string') {
            areaObjs.push({id: newT});
        } else {
            for (var i = 0; i < newT.length; i++) {
                areaObjs.push({id: newT[i]});
            }

        }

    }
    if (island) {
        if (typeof island === 'string') {
            areaObjs.push({id: island});
        } else {
            for (var i = 0; i < island.length; i++) {
                areaObjs.push({id: island[i]});
            }
        }
    }
    console.log("areaObjs: " + areaObjs.toString());

    var tempSubject = [];
    if (core) {
        if (typeof core === 'string') {
            tempSubject.push({id: core});
        } else {
            for (var i = 0; i < core.length; i++) {
                tempSubject.push({id: core[i]});
            }
        }
    }

    if (elective) {
        if (typeof elective === 'string') {
            tempSubject.push({id: elective});
        } else {
            for (var i = 0; i < elective.length; i++) {
                tempSubject.push({id: elective[i]});
            }
        }
    }


    var tempUpdate = {
        //_creator: req.user._id,
        //_id: mongoose.Types.ObjectId(),
        student_area: areaObjs,
        tutor_gender_requirement: tutorGender,
        student_days: tempDays,
        student_gender: studentGender,
        student_level: studentLevel,
        student_expected_hr_price: studentExpectedHrPrice,
        student_subject: tempSubject,

        other_requirement: otherRequirement

    }
    if (!isNaN(startTimeStamp)) {
        tempUpdate.start_time = startTimeStamp;
    }
    if (!isNaN(endTimeStamp)) {
        tempUpdate.end_time = endTimeStamp;
    }

    Student.update({_id: req.body.id}, {
        $set: tempUpdate
    }).exec(function (err) {
        console.log("message:" + err);
        endFunc(res, "/manage-students");
    })
}
