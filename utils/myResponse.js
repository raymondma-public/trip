/**
 * Created by HEI on 25/8/2015.
 */

exports.render = function (res, data) {
    if(res.statusCode==404){
        res.redirect("/404");
    }

    console.log("data:" + data.url);
    res.render(data.url, {data: data});
}


exports.redirect = function (res, url, view, data) {
    if(res.statusCode==404){
        res.redirect("/404");
    }
    res.redirect(url);
}


exports.send = function (res, url, view, data) {
    //console.log("data:" + data.doc);

    res.json(data);
}