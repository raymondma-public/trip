/**
 * Created by RaymondMa on 2/11/2015.
 */
var mongoose = new require('mongoose');

exports.getUser = function (req, res, endFunc) {
    var params=req.query;
    console.log(params.access_token);

    User.findOne({"login_token.token":params.access_token},function(err,user){
        console.log("user:"+user);
        if(err || !user ){
            res.statusCode=404;
            res.send({message:"Cannot Get Users"});
        }else{

            res.send(user);
        }
    })

}