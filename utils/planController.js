/**
 * Created by RaymondMa on 2/11/2015.
 */

var mongoose = new require('mongoose');
exports.createPlan = function (req, res, endFunc, body) {
    console.log("createPlan");
    //var noOfDays = req.body.noOfDays;
    //var planedEvent = req.body.planedEvent;
    //var rating = [];
    //var viewed = [];
    //var blogs = [];
    //var title = req.body.title;
    //var description=req.body.description;
    //var date = new Date().getTime();
    //var token = req.body.access_token;


    value = JSON.parse(body);


    var noOfDays = value.noOfDays;
    var planedEvent = value.planedEvent;
    var rating = [];
    var viewed = [];
    var blogs = [];
    var title = value.title;
    var description = value.description;
    var date = new Date().getTime();
    var token = value.access_token;
    console.log(token);


    User.findOne({"login_token.token": token}, function (err, user) {
        if (err || !user) {
            res.statusCode = 404;
            res.send({message: "unauthorized"});
            return
        } else {


            var tempRecord = new Plan({
                //_creator: req.user.id,
                _id: mongoose.Types.ObjectId(),
                title: title,
                noOfDays: noOfDays,
                //user_type: role,
                planedEvent: planedEvent,
                //user_dob: dob,
                rating: rating,
                viewed: viewed,
                blogs: blogs,
                createTime: date,
                createBy: user._id,
                description: description

            });


            tempRecord.save(function (err) {
                if (err) {
                    console.log('Error on save! ' + err);
                    res.statusCode = 404;
                    endFunc(res, null, null, {message: "Error:" + err.errmsg,})
                    //res.redirect("register-page?warning=" + true);
                    //endFunc(res, "/login-again?regSuccessful=true", null, {message: "OK"});

                } else {
                    //res.send("OK");

                    endFunc(res, null, null, {message: "OK"});


                }
            });


        }
    })


}


exports.getPlan = function (req, res, endFunc) {
    var params = req.query;
    console.log("_id:" + params.id);
    Plan.findOne({_id: params.id}, function (err, plan) {//_id:params.id
        if (err || !plan) {
            res.statusCode = 404;
            res.send({message: "Not Found"});
        } else {
            endFunc(res, null, null, plan);
        }

    })
}

exports.getPlanList = function (req, res, endFunc) {

    Plan.find({}).populate("planedEvents").exec(function (err, planList) {//_id:params.id
        if (err || !planList) {
            res.statusCode = 404;
            res.send({message: "Not Found"});
        } else {

            var options = {
                path: "planedEvents.event",
                model: "event"

            }
            Plan.populate(planList, options, function (err, planList) {

                if (err || !planList) {
                    res.statusCode = 404;
                    res.send({message: "Not Found"});
                } else {

                    var tempPlanList = JSON.parse(JSON.stringify(planList));
                    for (var i = 0; i < tempPlanList.length; i++) {

                        var tempPlan = tempPlanList[i];
                        tempPlanList[i].allImgURLs = [];
                        tempPlan.allImgURLs = [];
                        //console.log("tempPlan: "+tempPlan);
                        //console.log("tempPlan.planedEvents.length: "+tempPlan.planedEvents.length);

                        if (tempPlan.planedEvents) {
                            for (var j = 0; j < tempPlan.planedEvents.length; j++) {
                                var event = tempPlan.planedEvents[j].event;

                                if (event) {
                                    console.log("event.imgs.length: " + event.imgs.length);
                                    for (var k = 0; k < event.imgs.length; k++) {
                                        tempPlan.allImgURLs.push(event.imgs[k].srcURL);

                                        tempPlanList[i].allImgURLs.push(event.imgs[k].srcURL);
                                        console.log("allImgURLs:" + tempPlanList[i].allImgURLs);
                                    }
                                }

                            }
                        }

                    }
                    //
                    //for(var i=0;i<planList.length;i++){
                    //    var tempPlan=planList[i];
                    //
                    //    console.log("plan: "+i);
                    //    console.log("tempPlan.allImgURLs.length: "+tempPlan.allImgURLs.length);
                    //    for( var j=0;j<tempPlan.allImgURLs.length;j++){
                    //        console.log("allImgURLs: "+tempPlan.allImgURLs[j]);
                    //    }
                    //}


                    //var tempPlanList=JSON.parse(JSON.stringify(planList));
                    ////tempPlanList=[{},{},{}]
                    //
                    //for(var i=0;i<tempPlanList.length;i++){
                    //    var tempPlan=tempPlanList[i];
                    //        tempPlan.allImgURLs=[];
                    //}

                    endFunc(res, null, null, {data: tempPlanList});//{data: planList}
                }
            })


        }

    })
}


exports.updatePlan = function (req, res, endFunc) {


    var params = req.params;
    var id = params.id;
    var noOfDays = params.noOfDays;
    var planedEvent = params.planedEvent;
    var rating = params.rating;
    var viewed = [];
    var blogs = [];


    Plan.findOne({_id: id}).exec(function (err, plan) {
        var date = new Date().getTime();

        plan = new Plan({
            //_creator: req.user.id,
            _id: mongoose.Types.ObjectId(),
            noOfDays: noOfDays,
            //user_type: role,
            planedEvent: planedEvent,
            //user_dob: dob,
            rating: {
                who_rate: req.user,
                rate: rating
            },
            viewed: viewed,
            blogs: blogs,
            createTime: date,
            createBy: req.user._id

        });


        tempRecord.save(function (err) {
            if (err) {
                console.log('Error on save! ' + err);
                res.statusCode = 404;
                endFunc(res, null, null, {message: "Error:" + err.errmsg,})
                //res.redirect("register-page?warning=" + true);
                //endFunc(res, "/login-again?regSuccessful=true", null, {message: "OK"});

            } else {
                //res.send("OK");

                endFunc(res, null, null, {message: "OK"});


            }
        });

    })


}


exports.delete = function (req, res, endFunc) {
    var noOfDays = req.body.noOfDays;
    var planedEvent = [];
    var rating = [];
    var viewed = [];
    var blogs = [];


    var date = new Date().getTime();

    var tempRecord = new Plan({
        //_creator: req.user.id,
        _id: mongoose.Types.ObjectId(),
        noOfDays: noOfDays,
        //user_type: role,
        planedEvent: planedEvent,
        //user_dob: dob,
        rating: rating,
        viewed: viewed,
        blogs: blogs,
        createTime: date,
        createBy: req.user._id

    });


    tempRecord.save(function (err) {
        if (err) {
            console.log('Error on save! ' + err);
            res.statusCode = 404;
            endFunc(res, null, null, {message: "Error:" + err.errmsg,})
            //res.redirect("register-page?warning=" + true);
            //endFunc(res, "/login-again?regSuccessful=true", null, {message: "OK"});

        } else {
            //res.send("OK");

            endFunc(res, null, null, {message: "OK"});


        }
    });
}
