/**
 * Created by RaymondMa on 2/11/2015.
 */
var mongoose = new require('mongoose');

var myUtil = require('./my-util');
exports.createEvent = function (req, res, endFunc) {
    //var params = req.body
    //var imgs = [];
    //var title=params.title;
    console.log("createEvent");

    var body = "";
    req.on("data", function (data) {
        body += data.toString();
    })


    req.on("end", function () {

        console.log(body);
        value = JSON.parse(body);
        var params = value
        var imgs = [];
        var title = params.title;

        if (myUtil.isArray(params.imgs)) {
            for (var i = 0; i < params.imgs.length; i++) {
                imgs.push({srcURL: params.imgs[i]});
            }
        } else {
            imgs.push({srcURL: params.imgs});
        }
        var description = params.description;

        var rating = [];
        var viewed = [];


        var date = new Date().getTime();

        var location=params.location;
        //var location = {
        //    locationName: params.locationName,
        //    latitutde: params.latitude,
        //    longitude: params.longitude
        //}

        var eventType = [];
        if (myUtil.isArray(params.eventType)) {
            for (var i = 0; i < params.eventType.length; i++) {
                eventType.push(params.eventType[i]);
            }
        } else {
            eventType.push(params.eventType);
        }

        var stdStartTime = params.stdStartTime;
        var stdEndTime = params.stdEndTime;
        var phone = params.phone;
        var traffic = params.traffic;
        var tips = params.tips;
        var token = params.access_token;

        User.findOne({"login_token.token": token}, function (err, user) {
            if (err || !user) {
                res.statusCode = 404;
                res.send("unauthorized");
                return
            } else {
                var tempRecord = new Event({


                    _id: mongoose.Types.ObjectId(),
                    title: title,
                    eventType: eventType,
                    whoWent: [],
                    location: location,
                    description: description,

                    imgs: imgs,


                    stdStartTime: stdStartTime,
                    stdEndTime: stdEndTime,
                    phone: phone,
                    traffic: traffic,
                    tips: tips,
                    rating: rating,
                    viewed: viewed,

                    createTime: date,
                    createdBy: user._id

                });


                tempRecord.save(function (err) {
                    if (err) {
                        console.log('Error on save! ' + err);
                        res.statusCode = 404;
                        endFunc(res, null, null, {message: "Error:" + err.errmsg,})
                        //res.redirect("register-page?warning=" + true);
                        //endFunc(res, "/login-again?regSuccessful=true", null, {message: "OK"});

                    } else {

                        endFunc(res, null, null, {message: "OK"});


                    }
                });
            }
        })
    })
}


exports.getEvent = function (req, res, endFunc) {
    var params = req.query;
    console.log("_id:" + params.id);
    Event.findOne({_id: params.id}, function (err, event) {//_id:params.id
        if (err || !event) {
            res.statusCode = 404;
            res.send({message: "Not Found"});
        } else {
            endFunc(res, null, null, event);
        }

    })
}


exports.getEventList = function (req, res, endFunc) {
    var params = req.query;
    console.log("_id:" + params.id);
    Event.find({}, function (err, event) {//_id:params.id
        if (err || !event) {
            res.statusCode = 404;
            res.send({message: "Not Found"});
        } else {
            endFunc(res, null, null, {data: event});
        }

    })
}