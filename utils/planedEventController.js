/**
 * Created by RaymondMa on 2/11/2015.
 */
var mongoose = new require('mongoose');



exports.createPlanedEvent = function (req, res, endFunc) {
    //var eventId = req.body.eventId;
    //var desigedStartTime = req.body.desigedStartTime;
    //var desigedEndTime = req.body.desigedEndTime;
    //var belongsToPlanId= req.body.belongsToPlanId;
    //
    //var token = req.body.access_token;



    var body = "";
    req.on("data", function (data) {
        body += data.toString();
    })


    req.on("end", function () {

        value = JSON.parse(body);
        var eventId = value.eventId;
        var desigedStartTime = value.desigedStartTime;
        var desigedEndTime = value.desigedEndTime;
        var belongsToPlanId = value.belongsToPlanId;

        var token = value.access_token;


        User.findOne({"login_token.token": token}, function (err, user) {
            if (err || !user) {
                res.statusCode = 404;
                res.send("unauthorized");
                return
            } else {


                var tempRecord = new PlanedEvent({
                    //_creator: req.user.id,
                    _id: mongoose.Types.ObjectId(),
                    event: eventId,
                    desigedStartTime: desigedStartTime,
                    designedEndTime: desigedEndTime

                });


                tempRecord.save(function (err) {
                    if (err) {
                        console.log('Error on save! ' + err);
                        res.statusCode = 404;
                        endFunc(res, null, null, {message: "Error:" + err.errmsg,})
                        //res.redirect("register-page?warning=" + true);
                        //endFunc(res, "/login-again?regSuccessful=true", null, {message: "OK"});

                    } else {
                        //res.send("OK");

                        Plan.findOne({_id: belongsToPlanId}, function (err, plan) {

                            plan.planedEvents.push(tempRecord);
                            plan.save(function (err) {
                                if (err) {
                                    res.send({message: "Error on save! "});
                                }
                                else {
                                    endFunc(res, null, null, {message: "OK"});
                                }
                            })

                        })


                    }
                });


            }
        })
    })

}


exports.getPlanedEvent = function (req, res, endFunc) {
    var params = req.query;
    console.log("_id:" + params.id);
    PlanedEvent.findOne({_id: params.id}, function (err, plan) {//_id:params.id
        if(err || !plan){
            res.statusCode=404;
            res.send({message:"Not Found"});
        }else{
            endFunc(res, null, null,{data: plan});
        }

    })
}
