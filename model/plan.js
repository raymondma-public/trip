/**
 * Created by RaymondMa on 29/10/2015.
 */
var mongoose = require('mongoose'), Schema = mongoose.Schema;
module.exports = function (app) {
    //var mongoose = require("mongoose");


    planSchema = new mongoose.Schema(
        {
            _id:String,
            title:String,
            noOfDays: Number,
            planedEvents: [{type: Schema.ObjectId, ref: 'planedEvent'}],
            description:String,
            rating: [{
                who_rate: {type: Schema.ObjectId, ref: 'user'},
                rate: Number
            }],
            viewed: [{type: Schema.ObjectId, ref: 'user'}],
            blogs: [{type: Schema.ObjectId, ref: 'blogs'}],
            createTime:Number,

            createBy:{type: Schema.ObjectId, ref: 'user'}

        }
    );

    //Plan=mongoose.model('plan', planSchema);

}