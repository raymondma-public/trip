/**
 * Created by RaymondMa on 29/10/2015.
 */
var mongoose = require('mongoose'), Schema = mongoose.Schema;
module.exports = function (app) {
    //var mongoose = require("mongoose");


    planedEventSchema = new mongoose.Schema(
        {
            _id:String,
            event:{type: Schema.ObjectId, ref: 'planedEvent'},
            desigedStartTime:Number,
            designedEndTime:Number

        }
    );


    PlanedEvent = mongoose.model('planedEvent', planedEventSchema);
}