/**
 * Created by RaymondMa on 29/10/2015.
 */
var mongoose = require('mongoose'), Schema = mongoose.Schema;
module.exports = function (app) {
    //var mongoose = require("mongoose");


    eventSchema = new mongoose.Schema(
        {
            _id: String,
            title:{type: String, unique: true},
            eventType: [String],
            whoWent: [{type: Schema.ObjectId, ref: 'user'}],
            location: {
                locationName: String,
                latitutde: Number,
                longitude: Number
            },
            description: String,
            imgs: [{srcURL: String}],
            stdStartTime: Number,
            stdEndTime: Number,
            phone: String,
            traffic: String,
            tips: String,
            rating: {
                who_rate: {type: Schema.ObjectId, ref: 'user'},
                rate: Number
            },
            viewed: [{type: Schema.ObjectId, ref: 'user'}],

            createTime:Number,
            createdBy:{type: Schema.ObjectId, ref: 'user'}

        }
    );


    Event=mongoose.model('event', eventSchema);
}