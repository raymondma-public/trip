/**
 * Created by RaymondMa on 29/10/2015.
 */

var mongoose = require('mongoose'), Schema = mongoose.Schema;
module.exports = function (app) {
    //var mongoose = require("mongoose");


    blogSchema = new mongoose.Schema(
        {
            _id: String,
            title:{type: String, unique: true},
            imgs: [{srcURL: String}],
            description: String,
            createTime: Number,
            createdBy:{type: Schema.ObjectId, ref: 'user'},
            viewed: [{type: Schema.ObjectId, ref: 'user'}],
            rating: [{
                who_rate: {type: Schema.ObjectId, ref: 'user'},
                rate: Number
            }],
        }
    );


    Blog = mongoose.model('blog', blogSchema);

}