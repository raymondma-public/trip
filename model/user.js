/**
 * Created by HEI on 26/7/2015.
 */
/**
 * Created by HEI on 7/6/2015.
 */

var password = require('password-hash-and-salt');
var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose'), Schema = mongoose.Schema;
module.exports = function (user) {
    //var mongoose = require("mongoose");


    userSchema = new mongoose.Schema(
        {

            _id: String,
            login_token: [{token: String, create_time: Number}],
            //email:{type: String, unique: true},
            user_password_hash: String,
            creditCard: String,
            created_plan: [{type: Schema.ObjectId, ref: 'planedEvent'}],
            user_phone: Number,

            user_name: {type: String, unique: true},


        });

    User = mongoose.model('tutorial-collection', userSchema);

    userSchema.methods.generateHash = function (password) {
        console.log("generateHash: " + generateHash);
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    };

//userSchema.prototype.generateHash = function (password) {
//    console.log("generateHash: " + generateHash);
//    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
//};


    userSchema.methods.validPassword = function (user_password) {
        //console.log("validPassword: " + validPassword);
        return bcrypt.compareSync(user_password, this.user_password_hash);
        // Verifying a hash
        //password(user_password).verifyAgainst(this.user_password_hash, function (error, verified) {
        //    //if (error)
        //    //    throw new Error('Something went wrong!');
        //    if (!verified) {
        //        console.log("Don't try! We got you!");
        //        return false;
        //    } else {
        //        console.log("The secret is...");
        //        return true;
        //    }
        //
        //
        //});
    };


    userSchema.methods.findByToken = function (token, cb) {
        console.log("findByToken");
        User.find({'login_token.token': token}).
            exec(function (err, user) {
                return cb(null, record);
            })

        //process.nextTick(function() {
        //    for (var i = 0, len = records.length; i < len; i++) {
        //        var record = records[i];
        //        if (record.token === token) {
        //            return cb(null, record);
        //        }
        //    }
        //    return cb(null, null);
        //});
    }
}